# 云 API 设置

### 概述

云API 设置是设置腾讯云设置所有功能项的基础条件，配置云API的密钥后，才可使用腾讯云的各项服务和能力。本文将指导您如何设置并开启 Discuz! Q “云API” 设置。

### 操作指南

### 步骤1:获取访问密钥
1. 使用您的腾讯云账号登录腾讯云[访问管理](https://console.cloud.tencent.com/cam/capi)。
2. 在访问管理页面依次单击【API密钥】>【密钥管理】。即可获取到您的 `Secretid` 与 `SecretKey`信息。如下图所示：

![](https://main.qcloudimg.com/raw/9dd23d3dfa5893500a1cad0b5c86f38b.png)

### 步骤2:云 API 设置 

1. 使用 `http(s)://{站点名称}/admin` 登录 Discuz! Q 站点后台。
2. 在 Discuz! Q 站点后台中依次单击【全局】>【腾讯云设置】。
3. 单击 “云API” 处【配置】。如下图所示：

![](https://main.qcloudimg.com/raw/028b5b8e83c836eaacba6ee7357963ff.png)

4. 在云API配置处，填写您的 `Secretid` 与 `SecretKey`。如下图所示：

![](https://main.qcloudimg.com/raw/aa68135f78bf06118e8a01383371282d.png)

::: tip
`Secretid` 与 `SecretKey`获取操作，详情请参见：获取访问密钥。
:::

5. 单击【提交】。
6. 提交成功后，返回腾讯云设置页面。在腾讯云设置页面中，单击【云 API】处【开启】按钮。如下图所示：

![](https://main.qcloudimg.com/raw/666d528a5177eb7e9a2ee8682ca3aa02.png)

7. 设置页面弹出“修改成功”后，即可完成 云API 设置操作。如下图所示：

![](https://main.qcloudimg.com/raw/3dcfcef146dcfa7d5fd563b026ae4309.png)





