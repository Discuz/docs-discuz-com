---
sidebarDepth: 2
---
# Windows 主机安装
## 基于手动配置的环境
###  Apache

####  操作场景

本文档将指导您如何在已安装 Apache 服务与 PHP 的 Windows Server 服务器上安装部署 Discuz! Q。

::: tip
- 若您已知晓如何安装部署 Discuz! Q，您可以直接[单击此处](https://dl.discuz.chat/dzq_latest_install.zip)下载 Discuz! Q 进行安装部署。
- 本文档以 Apache 2.4 、PHP 7.3.1  版本为例。系统为 Windows Server 2016 。
::: 

####  前提条件

- 服务器已安装的 PHP 版本为 7.2 及以上(暂不推荐使用7.4)，数据库使用 MySQL 5.7.9 版本以上或 MariaDB 10.2 以上。
- 已成功登录 Windows Server 服务器。    

#### 操作步骤

#### 步骤一：配置PHP

**安装扩展**

::: tip

- Discuz! Q 依赖于 `BCMath` `Ctype` `Curl` `Dom` `Fileinfo` `GD` `JSON` `Mbstring` `Exif` `OpenSSL` `PDO` `PDO_mysql` `Tokenizer` `XML` `Zip`扩展插件，在 PHP 中需开启以上扩展。
- 以下操作因为系统版本，软件版本的不同，操作上会有所差异，请根据您的具体情况进行开启对应函数，以下操作仅提供示例。
  ::: 

**开启 exif 扩展:**

1. 您可以使用文本编辑器，打开 `php.ini `文件。
2. 编辑 `php.ini`  文件，去掉以下字段前面的 `; `分号，并保存 `php.ini`  文件。如下：

```
extension=exif    
exif.encode_unicode = ISO-8859-15
exif.decode_unicode_motorola = UCS-2BE
exif.decode_unicode_intel    = UCS-2LE
exif.encode_jis =
exif.decode_jis_motorola = JIS
exif.decode_jis_intel    = JIS=
```

3. 重启 Apache 与 PHP 服务。

::: tip
重启后如果扩展不生效，请确保 `php` 目录下 `ext` 文件夹具备对应扩展文件，`extension_dir `指向 `ext `文件夹并已去掉字段前 `;` 分号。如： `extension_dir = "C:\Program Files\php-7.3.1\ext"` 。
::: 

**启用 PHP 函数**

::: tip

- Discuz！Q 依赖于 `symlink`、`readlink`、`putenv`、`realpath`、`shell_exec` 函数，在PHP中需开启以上函数。
- 以下操作因为系统版本，软件版本的不同，操作上和命令上会有所差异，请根据您的具体情况开启对应函数 ，以下操作仅提供示例。
  ::: 

1. 打开并编辑 `php.ini`文件。查找 `disable_functions`字段，并删除禁用掉的`symlink`、`readlink`、 `putenv`、 `realpath`、`shell_exec` 函数。如下所示：

```
disable_functions = passthru,exec,system,chroot,chgrp,chown,shell_exec,proc_open,proc_get_status,popen,ini_alter,ini_restore,dl,openlog,syslog,popepassthru,stream_socket_server
```

::: tip
如果`disable_functions`字段中已删除对应函数，可忽略此操作。
:::

2. 保存 `php.ini`文件。重启 Apache 与 PHP 服务。

#### 步骤二：配置 Apache

::: tip

- Discuz！Q 依赖于 `mod_rewrit` 模块。请确保已经启用 `mod_rewrite`，以下操作将指导您启动 `mod_rewrite`。
- 以下操作因为系统版本，软件版本的不同，操作上和命令上会有所差异，请根据您的具体情况开启 `mod_rewrite` ，以下操作仅提供示例。
  :::

**启用 mod_rewrite**

1. 可以在 `conf` 目录的 `httpd.conf` 文件中，查找以下字段，并去掉字段前`#` 符号。如下所示：

```
LoadModule rewrite_module modules/mod_rewrite.so
```

2. 定位到 `<directory />` 字段，并修改内容为如下内容：

```
<directory />
        Options All
        AllowOverride All
        DirectoryIndex index.php index.html
</directory>
```

3. 保存 `php.ini`文件并重启 Apache 服务。

####  步骤三：下载并解压 Discuz！Q 安装包

1. 您可以通过服务器中的浏览器使用下载地址 `https://dl.discuz.chat/dzq_latest_install.zip` 下载 Discuz！Q 安装包，并解压至于网站目录。如下图所示： 

![img](https://main.qcloudimg.com/raw/b05fac26796c9325836cd4ada9ca86f1.png)

::: tip
该网站目录仅提供示例，请根据自己的环境与实际需求将 Discuz！Q 安装包解压至对应文件夹，一般为`www`文件夹。
:::

**步骤四：配置运行目录**

::: tip

- 完成以上配置后，您还需将运行目录设置为 Discuz！Q 网站文件的子目录的 `public` 文件夹。

- 以下 `public` 目录路径，仅提供示例，请根据自己的实际情况进行填写。

  ::: 

  打开并编辑 `php.ini`文件。查找 `DocumentRoot`字段，将运行目录设置为 Discuz！Q 网站文件的子目录的`public`文件夹。如下所示：

```
DocumentRoot "C:\Program Files\www\public"
```

#### 步骤五：初始化安装 Discuz! Q

1. 打开本地浏览器，访问 `http://<绑定网站的域名名称>/dl.php` 。如下图所示：

![img](https://main.qcloudimg.com/raw/d571ed3234565af42c508666b827ca37.png)

::: tip

- 站点如需部署 SSL 证书，使用 HTTPS 协议进行站点访问。建议您在以下操作之前部署安装 SSL 证书，并使用`https://<绑定网站的域名名称>/dl.php`  进行访问。部署安装 SSL 证书详情操作请参考： [Apache 服务器证书安装](https://cloud.tencent.com/document/product/400/35243)。
- 如需购买 SSL 证书，可参考 [SSL 证书购买流程](https://cloud.tencent.com/document/product/400/7995)，请根据您的实际情况进行购买相关证书。腾讯云为 Discuz! Q 站点提供免费 SSL 证书申请。详情请查看：[域名型（DV）免费证书申请流程](https://cloud.tencent.com/document/product/400/6814)
  ::: 

2. 单击【下一步】，Discuz! Q 将自行进行站点检查。如有报错等问题，可参考 [常见问题](https://discuz.com/docs/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98.html) 进行排查处理。
3. Discuz! Q 检查完成后，需配置网站相关信息。如下图所示：

![img](https://main.qcloudimg.com/raw/8e3ac52669180620b946750787b50c95.png)

- **站点名称**：请输入您的站点名称信息，可自定义。

- **MySQL 服务器地址**：请输入您的 MySQL 服务器地址。

- **数据库名称**：请输入您的数据库名称。

- **MySQL 用户名**：请输入您的数据库用户名。

- **MySQL 密码**：请输入您的数据库密码。

- **表前缀**：可选，可自定义数据库表前缀名称。默认不填。

- **设置管理员用户名**：请输入您 Discuz! Q 站点的管理员用户名。

- **设置管理员密码**：请输入您 Discuz! Q 站点的管理员密码。

- **管理员密码确认**：请再次输入您 Discuz! Q 站点的管理员密码。

4. 单击【下一步】。即可完成 DIscuz！Q 的安装部署。

#### 步骤七：添加计划任务

为保证 DIscuz！Q 站点功能的正常使用。请添加一个每分钟运行一次的计划任务，脚本内容为以下命令。其中的 `PHP文件目录` 和 `网站主目录` 请按自己的实际情况做相应调整。

```
c:\<PHP文件目录>\php.exe c:\<网站主目录>\disco schedule:run
```

###  Nginx
####  操作场景

本文档将指导您如何在已安装 Nginx 服务的 Windows Server 服务器上安装部署 Discuz! Q。

::: tip
- 若您已知晓如何安装部署 Discuz! Q，您可以直接[单击此处](https://dl.discuz.chat/dzq_latest_install.zip)下载 Discuz! Q 进行安装部署。
- 本文档以 Nginx 1.18.0 、PHP 7.3.1  版本为例。系统为 Windows Server 2016 。
:::

####  前提条件

- 服务器已安装的 PHP 版本为 7.2 及以上(暂不推荐使用7.4)，数据库使用 MySQL 5.7.9 版本以上或 MariaDB 10.2 以上。
- 已成功登录 Windows Server 服务器。    

####  操作步骤
#### 步骤一：配置PHP

**安装扩展**

::: tip
- Discuz! Q 依赖于 `BCMath` `Ctype` `Curl` `Dom` `Fileinfo` `GD` `JSON` `Mbstring` `Exif` `OpenSSL` `PDO` `PDO_mysql` `Tokenizer` `XML` `Zip`扩展插件，在 PHP 中需开启以上扩展。
- 以下操作因为系统版本，软件版本的不同，操作上会有所差异，请根据您的具体情况进行开启对应函数，以下操作仅提供示例。
::: 

**开启 exif 扩展:**

1. 您可以使用文本编辑器，打开`php.ini`文件。
2. 编辑`php.ini` 文件，去掉以下字段前面的`;`分号，并保存`php.ini` 文件。如下：

```
extension=exif    
exif.encode_unicode = ISO-8859-15
exif.decode_unicode_motorola = UCS-2BE
exif.decode_unicode_intel    = UCS-2LE
exif.encode_jis =
exif.decode_jis_motorola = JIS
exif.decode_jis_intel    = JIS=
```

3. 重启 Nginx 与 PHP 服务。

::: tip
重启后如果扩展不生效，请确保 PHP 目录下`ext` 文件夹具备对应扩展文件，`extension_dir`指向`ext`文件夹并已去掉字段前`;`分号。如：`extension_dir = "C:\Program Files\php-7.3.1\ext"`。
::: 

**启用 PHP 函数**

::: tip
- Discuz！Q 依赖于 `symlink`、`readlink`、`putenv`、`realpath`、`shell_exec` 函数，在 PHP 中需开启以上函数。
- 以下操作因为系统版本，软件版本的不同，操作上和命令上会有所差异，请根据您的具体情况开启对应函数 ，以下操作仅提供示例。
::: 

1. 打开并编辑 `php.ini`文件。查找 `disable_functions`字段，并删除禁用掉的`symlink`、`readlink`、 `putenv`、 `realpath`、 `shell_exec` 函数。如下所示：

```
disable_functions = passthru,exec,system,chroot,chgrp,chown,shell_exec,proc_open,proc_get_status,popen,ini_alter,ini_restore,dl,openlog,syslog,popepassthru,stream_socket_server
```

::: tip
如果`disable_functions`字段中已删除对应函数，可忽略此操作。
:::

2. 保存 `php.ini` 文件并重启 Nginx 与 PHP 服务。


#### 步骤二：配置 Nginx

1. 进入 `nginx` 目录下 `conf` 目录，并找到 `nginx.conf`配置文件。如下图所示： ![img](https://main.qcloudimg.com/raw/6f589a23079fdf7a01793bad144c8010.png)

2. 使用文本编辑器打开 `nginx.conf` 配置文件并进行以下配置。

::: warning
Nginx 必须包含以下配置。
:::
请将`root`目录指向 `<站点主目录>/public`目录，同时一定要配置`index`和 `location /`，将所有的请求将引导到 `index.php` 。具体配置请参考如下：

#### root 配置
请确认 `root` 指向了安装好的 `public`目录，以下是示例，请按自己的实际配置设置。

```
root /home/www/discuz/public;
```

#### index 配置
请确认 index 的第一项是 index.php ，以下为示例。

```
index index.php index.html;
```

#### location 配置

请确认	`location /` 按如下配置，如果之前有相关配置，请替换：

```
    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }
```


#### Nginx 建议配置 
建议添加以下配置，以启用 gzip 压缩,减少服务器资源损耗。

```
  gzip on;
  gzip_min_length 1024;
  gzip_types application/json text/html text/css application/x-javascript application/javascript application/vnd.api+json;
  gzip_disable "MSIE [1-6]\.";
  gzip_comp_level 2;
```

3 .  重启 Nginx 服务。

#### 步骤三：下载并解压 Discuz！Q 安装包

1. 您可以通过服务器中的浏览器使用下载地址 `https://dl.discuz.chat/dzq_latest_install.zip` 下载 Discuz！Q 安装包，并解压至于网站目录。如下图所示： ![img](https://main.qcloudimg.com/raw/b05fac26796c9325836cd4ada9ca86f1.png)

::: tip
该网站目录仅提供示例，请根据自己的环境与实际需求将 Discuz！Q 安装包解压至对应文件夹，一般为`www`文件。
:::

#### 步骤四：初始化安装 Discuz! Q


1. 打开本地浏览器，访问 `http://<绑定网站的域名名称>/dl.php` 。如下图所示：

![img](https://main.qcloudimg.com/raw/d571ed3234565af42c508666b827ca37.png)

::: tip
- 站点如需部署 SSL 证书，使用 HTTPS 协议进行站点访问。请在以下操作之前部署安装 SSL 证书，并使用`https://<绑定网站的域名名称>/dl.php`  进行访问。部署安装 SSL 证书详情操作请参考： [Apache 服务器证书安装](https://cloud.tencent.com/document/product/400/35243)。
- 如需购买 SSL 证书，可参考 [SSL 证书购买流程](https://cloud.tencent.com/document/product/400/7995)，请根据您的实际情况进行购买相关证书。腾讯云为 Discuz! Q 站点提供免费 SSL 证书申请。详情请查看：[域名型（DV）免费证书申请流程](https://cloud.tencent.com/document/product/400/6814)
::: 

2. 单击【下一步】，Discuz! Q 将自行进行站点检查。如有报错等问题，可参考 [常见问题](https://discuz.com/docs/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98.html) 进行排查处理。
3. Discuz! Q 检查完成后，需配置网站相关信息。如下图所示：

![img](https://main.qcloudimg.com/raw/8e3ac52669180620b946750787b50c95.png)

- **站点名称**：请输入您的站点名称信息，可自定义。
- **MySQL 服务器地址**：请输入您的 MySQL 服务器地址。
- **数据库名称**：请输入您的数据库名称。
- **MySQL 用户名**：请输入您的数据库用户名。
- **MySQL 密码**：请输入您的数据库密码。
- **表前缀**：可选，可自定义数据库表前缀名称。默认不填。
- **设置管理员用户名**：请输入您 Discuz! Q 站点的管理员用户名。
- **设置管理员密码**：请输入您 Discuz! Q 站点的管理员密码。
- **管理员密码确认**：请再次输入您 Discuz! Q 站点的管理员密码。

4. 单击【下一步】。即可完成 DIscuz！Q 的安装部署。

#### 步骤五：添加计划任务

为保证 DIscuz！Q 站点功能的正常使用。请添加一个每分钟运行一次的计划任务，脚本内容为以下命令。其中的 `PHP文件目录` 和 `网站主目录` 请按自己的情况做相应调整。

```
c:\<PHP文件目录>\php.exe c:\<网站主目录>\disco schedule:run
```

### IIS 

#### 操作场景

本文档将指导您如何在已安装 IIS 服务的 Windows Server 服务器上安装部署 Discuz! Q。 

::: tip
- 若您已知晓如何安装部署 Discuz! Q，您可以直接[单击此处](https://dl.discuz.chat/dzq_latest_install.zip)下载 Discuz! Q 进行安装部署。
- 本文档以 PHP 7.3.1  版本为例。系统为 Windows Server 2016 。
  :::

#### 前提条件

- 服务器已安装的 PHP 版本为 7.2 及以上(暂不推荐使用7.4)，数据库使用 MySQL 5.7.9 版本以上或 MariaDB 10.2 以上。
- 已成功登录 Windows Server 服务器。    

#### 操作步骤

#### 步骤一：配置PHP

#### 安装扩展

::: tip

- Discuz! Q 依赖于 `BCMath` `Ctype` `Curl` `Dom` `Fileinfo` `GD` `JSON` `Mbstring` `Exif` `OpenSSL` `PDO` `PDO_mysql` `Tokenizer` `XML` `Zip`扩展插件，在 PHP 中需开启以上扩展。
- 以下操作因为系统版本，软件版本的不同，操作上会有所差异，请根据您的具体情况进行开启对应函数，以下操作仅提供示例。
  :::

#### 开启 `exif` 扩展:

1. 您可以使用文本编辑器，打开 `php.ini` 文件。
2. 编辑 `php.ini` 文件，去掉以下字段前面的 `;` 分号，并保存 `php.ini` 文件。如下：

```
extension=exif    
exif.encode_unicode = ISO-8859-15
exif.decode_unicode_motorola = UCS-2BE
exif.decode_unicode_intel    = UCS-2LE
exif.encode_jis =
exif.decode_jis_motorola = JIS
exif.decode_jis_intel    = JIS=
```

3. 重启  IIS 服务。

::: tip
重启后如果扩展不生效，请确保 `php` 目录下 `ext` 文件夹具备对应扩展文件，`extension_dir`指向`ext`文件夹并已去掉字段前`;`分号。如：`extension_dir = "C:\Program Files\php-7.3.1\ext"`。
:::

#### 启用 PHP 函数


::: tip

- Discuz！Q 依赖于 `symlink`、`readlink`、`putenv`、`realpath`、`shell_exec` 函数，在 PHP 中需开启以上函数。
- 以下操作因为系统版本，软件版本的不同，操作上和命令上会有所差异，请根据您的具体情况开启对应函数 ，以下操作仅提供示例。
  :::

1. 打开并编辑 `php.ini`文件。查找 `disable_functions` 字段，并删除禁用掉的 `symlink`、`readlink`、 `putenv`、 `realpath`、`shell_exec`  函数。如下所示：

```
disable_functions = passthru,exec,system,chroot,chgrp,chown,shell_exec,proc_open,proc_get_status,popen,ini_alter,ini_restore,dl,openlog,syslog,popepassthru,stream_socket_server
```

::: tip
如果`disable_functions`字段中已删除对应函数，可忽略此操作。
:::

2. 保存 `php.ini`文件并重启  IIS 服务。

#### 步骤二：配置 IIS

#### 配置URL Rewrite扩展模块：

1. 单击【[下载URL Rewrite扩展](https://go.microsoft.com/?linkid=9722532)】并安装该 URL Rewrite 扩展模块。

::: tip
安装该 URL Rewrite 扩展模块时，如果报错 `IIS Version 7.0 or greater is required to install IIS URLRewrite Module 2.` ,请 [下载最新版本](https://www.iis.net/downloads/microsoft/url-rewrite) 进行安装。
:::

2. 安装完成后，在IIS管理器网站主页，单击 【URL重写】 如下图所示：
   ![](https://main.qcloudimg.com/raw/6220e8205cc1af0924324ad1ffa3dcb2.png)

3. 添加以下规则。

```
<rule name="root_location_rewrite" stopProcessing="true">
  <match ignoreCase="false" url="."/>
  <conditions logicalGrouping="MatchAll">
    <add ignoreCase="false" input="{REQUEST_FILENAME}" matchType="IsFile" negate="true"/>
    <add ignoreCase="false" input="{REQUEST_FILENAME}" matchType="IsDirectory" negate="true"/>
  </conditions>
  <action appendQueryString="true" type="Rewrite" url="index.php?{QUERY_STRING}"/>
</rule>
<rule name="default_file_rewrite" stopProcessing="true">
  <match ignoreCase="false" url="^$"/>
  <action appendQueryString="true" type="Rewrite" url="index.php?{QUERY_STRING}"/>
</rule>
```

#### 配置默认文档

1. 在 IIS 管理器中，单击【默认文档】，如下图所示：

![](https://main.qcloudimg.com/raw/f9f4237286d3aa018735e94e1d1b8b78.png)

2. 在默认文档页，单击【添加】按钮，并设置名称为【index.php】的默认文档名称。如下图所示：

![](https://main.qcloudimg.com/raw/d12d040be6b53006b03da65552d11df0.png)

3. 单击【确认】，即可完成配置默认文档操作。

::: tip
IIS 管理器默认会添加 `index.html` 的默认文档，请检查是否已添加。未添加可参考以上操作进行添加，否则将可能导致多次刷新页面时报 403 错误。
::: 


#### 步骤三：下载并解压 Discuz！Q 安装包

1. 您可以通过服务器中的浏览器使用下载地址 `https://dl.discuz.chat/dzq_latest_install.zip` 下载 Discuz！Q 安装包，并解压至于网站目录。如下图所示：

![](https://main.qcloudimg.com/raw/76ee5b35922ea2ca9241b94734571ada.png)


::: tip
该网站目录仅提供示例，请根据自己的环境与实际需求将 Discuz！Q 安装包解压至对应文件夹，一般为`wwwroot`目录下。
::: 


#### 步骤四：配置运行目录

1. 在IIS管理器选择需要部署的网站名称，右击选择管理网站，并单击【高级设置】。
2. 在网站高级设置窗口中，选择【物理路径】，选择 Discuz！Q 部署文件的网站目录下的 `public` 子目录。如下图所示：

![](https://main.qcloudimg.com/raw/adaee39db63c8226ed3693f3654cb5f1.png)

3. 单击【确定】。

#### 步骤五：初始化安装 Discuz! Q

1. 打开本地浏览器，访问 `http://<绑定网站的域名名称>/dl.php` 。如下图所示：

![](https://main.qcloudimg.com/raw/d571ed3234565af42c508666b827ca37.png)


::: tip

- 站点如需部署 SSL 证书，使用 HTTPS 协议进行站点访问。请在以下操作之前部署安装 SSL 证书，并使用 `https://<绑定网站的域名名称>/dl.php`  进行访问。部署安装 SSL 证书详情操作请参考： [IIS 服务器证书安装](https://cloud.tencent.com/document/product/400/35225)。
- 如需购买 SSL 证书，可参考 [SSL 证书购买流程](https://cloud.tencent.com/document/product/400/7995)，请根据您的实际情况进行购买相关证书。腾讯云为 Discuz! Q 站点提供免费 SSL 证书申请。详情请查看：[域名型（DV）免费证书申请流程](https://cloud.tencent.com/document/product/400/6814)
  :::

2. 单击【下一步】，Discuz! Q 将自行进行站点检查，如有报错等问题，可参考 [常见问题](https://discuz.com/docs/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98.html) 进行排查处理。

3. Discuz! Q 检查完成后，需配置网站相关信息。如下图所示：

![](https://main.qcloudimg.com/raw/8e3ac52669180620b946750787b50c95.png)

- **站点名称**：请输入您的站点名称信息，可自定义。
- **MySQL 服务器地址**：请输入您的 MySQL 服务器地址。
- **数据库名称**：请输入您的数据库名称。
- **MySQL 用户名**：请输入您的数据库用户名。
- **MySQL 密码**：请输入您的数据库密码。
- **表前缀**：可选，可自定义数据库表前缀名称。默认不填。
- **设置管理员用户名**：请输入您 Discuz! Q 站点的管理员用户名。
- **设置管理员密码**：请输入您 Discuz! Q 站点的管理员密码。
- **管理员密码确认**：请再次输入您 Discuz! Q 站点的管理员密码。

4. 单击【下一步】。即可完成 DIscuz！Q 的安装部署。

#### 步骤七：添加计划任务

为保证 DIscuz！Q 站点功能的正常使用。请添加一个每分钟运行一次的计划任务，脚本内容为以下命令。其中的 `PHP文件目录` 和 `网站主目录` 请按自己的情况做相应调整。

```
c:\<PHP文件目录>\php.exe c:\<网站主目录>\disco schedule:run
```

## 基于宝塔（推荐）
####  操作场景
本文档将指导您如何在已安装宝塔面板的 Windows Server  服务器上安装部署 DIscuz！Q。

::: tip
- 若您已知晓如何安装部署 Discuz! Q，您可以直接[单击此处](https://dl.discuz.chat/dzq_latest_install.zip)下载 Discuz! Q 进行安装部署。
- 本文档以宝塔面板 6.9.0 版本、Windows Server 2012 R2 Datacenter  系统为例。 
::: 

#### 前提条件
- 宝塔面板安装的 PHP 版本需为 7.2 及以上(暂不推荐使用7.4)，数据库需使用 MySQL 5.7.9 版本以上或 MariaDB 10.2 以上。
- 已成功登录宝塔控制台。    


#### 操作步骤

#### 步骤一：配置PHP
#### 安装扩展
::: tip
Discuz！Q 依赖于 `fileinfo` 和 `exif` 两个扩展运行，部署前需对 PHP 进行相关配置。以下操作将指导您安装 `fileinfo` 和 `exif` 扩展。
::: 

1. 在宝塔控制台中，单击【软件管理】->【已安装】，查找已安装的 PHP 软件。如下图所示： 

![](https://main.qcloudimg.com/raw/399e2a061911e109100b023dd13c3fe2.png)

2. 单击选择需要为 Discuz！Q 使用的 PHP 软件名称（需为7.2及以上）。此处以 PHP-7.3 为例。

![](https://main.qcloudimg.com/raw/5358b5ea9eab9ee50995a2fd22604802.png)

3. 在弹出的 【php 管理】窗口中，单击【安装扩展】，进行安装扩展设置。如下图所示

![](https://main.qcloudimg.com/raw/04e21462ea9525e6d9735da890e85d24.png)

4.  选择【fileinfo】与【exif】，并单击【安装】。如下图所示：

![](https://main.qcloudimg.com/raw/35c37af44765699060f45a2b3f672df5.png)


5. 【php 管理】窗口中【fileinfo】与【exif】状态栏显示为![](https://main.qcloudimg.com/raw/6afdf7dacebf13ae1615976c36da021f.png)即为安装成功。

#### 删除禁用函数
::: tip
Discuz！Q 需删除禁用的函数 `putenv`、`readlink`、`symlink`、 `shell_exec`  ，部署前需对 PHP 进行相关配置。以下操作将指导您删除禁用函数 `putenv`、`readlink`、`symlink`、`shell_exec`  。
::: 

1. 在 【php 管理】窗口中，单击【禁用函数】，进入禁用函数设置页面。如下图所示：

![](https://main.qcloudimg.com/raw/0c31e34664a77eeb07cc3a517b19dae3.png)

2. 在函数列表中单击【删除】函数 `putenv`、`readlink`、`symlink`、`shell_exec`，即可删除禁用的函数。

#### 配置 openssl
1. 在宝塔控制台中，单击【文件】->【新建】->【新建目录】。如下图所示： 

![](https://main.qcloudimg.com/raw/d2b196e23eae562815d29be8d41aeba2.png)

2. 在弹出的【新建目录】窗口中，输入 `\usr\local\ssl\` 创建目录，并单击【保存】。如下图所示： 

![](https://main.qcloudimg.com/raw/aa3b4790e87c775d2ec0cb6e0895f0a9.png)

3. 在宝塔文件管理器地址栏中，输入 `C:/BtSoft/php/73/extras/ssl` ，勾选 `openssl.cnf` 文件，单击【剪切】。如下图所示： 

![](https://main.qcloudimg.com/raw/bbd8de8d633a7d6c4af63dbbf8e9f1b5.png)


4. 在宝塔文件管理器地址栏中，输入刚创建的 `C:\usr\local\ssl\` 目录 。单击【粘贴】。将 `openssl.cnf` 文件放置在 `ssl`目录下。如下图所示： 

![](https://main.qcloudimg.com/raw/11f23ce7fd128bff9cbfb471a4f87316.png)

5. 在宝塔文件管理器中，单击【远程下载】，在弹出的下载文件窗口中，输入以下信息。如下图所示： 

![](https://main.qcloudimg.com/raw/7dcafa6a0b1b89332663498973b38bd2.png)

- URL地址：请输入 `https://dl.discuz.chat/mirrors/cacert.pem`,以下载`cacert.pem`文件。
- 下载到：请输入 `C:`。
- 文件名：`cacert.pem`，无需填写，系统将自动填充。

6. 单击【确定】。

7. 在宝塔控制台中，单击【软件管理】->【已安装】，单击选择需要为 Discuz！Q 使用的 PHP 软件名称。
8. 在弹出的【php管理】弹窗中，单击【配置文件】。并修改 PHP 配置文件，将 `curl.cainfo` 和 `openssl.cafile` 设置为 `C:\cacert.pem`，并去掉前面的 `;` 符号。配置信息请参考如下：

```
curl.cainfo=C:\cacert.pem
openssl.cafile=C:\cacert.pem
```

#### 步骤二：创建站点

1. 在宝塔控制台中，单击【网站】->【添加站点】。如下图所示： 

![](https://main.qcloudimg.com/raw/55f6d21e8151713e07cb3b3371a008c2.png)
2. 在弹出的添加网站窗口中，输入相关配置信息。如下图所示：

![](https://main.qcloudimg.com/raw/ddea8e6c25e657a6ad23142cd48e9f8c.png)

- **域名**：输入绑定 Discuz！Q 域名。
::: tip
确保绑定的域名已添加相关解析。如您的域名使用腾讯云 DNS 解析 DNSPod ，详情操作可参考：[快速添加域名解析](https://cloud.tencent.com/document/product/302/3446)。
::: 

- **备注**：可选，可添加网站的备注信息。
- **根目录**：网站文件根目录。一般情况使用默认`C:/wwwroot`路径。
- **FTP**：可选，可创建 FTP 服务。默认不创建。
- **数据库**：可选，您可在您的服务器内创建数据库进行使用。也可以其他数据库,数据库类型需为 MySQL 5.7.9 版本以上或MariaDB 10.2 以上。如：[云数据库 TencentDB for MySQL](https://cloud.tencent.com/product/cdb)。详情操作请参考：[云数据库 MySQL 产品文档](https://cloud.tencent.com/document/product/236) 。
::: tip
如需在服务器内创建数据库进行使用，请选择【MySQL】,【utf8mb4】，并输入数据库用户名以及密码。
::: 

- 程序类型：PHP，默认不可选。
- PHP版本：请选择已配置完成的 PHP 版本。此处为 PHP-73。
- 网站分类：默认分类。可根据实际情况进行选择。

3.单击【提交】。即可创建站点。

#### 步骤三：远程下载 DIscuz！Q 部署文件压缩包

1. 单击已创建的 Discuz！Q 根目录路径，如下图所示：
![](https://main.qcloudimg.com/raw/2cb0cda984fd895e6b275986290ab7f8.png)

2. 在文件目录中，单击【远程下载】。在弹出的下载文件窗口中输入相关信息。如下图所示：

![](https://main.qcloudimg.com/raw/02a1d930d37a616f6470d460d1da1b42.png)

- URL地址：请输入 Discuz！Q 下载地址： `https://dl.discuz.chat/dzq_latest_install.zip`。
- 下载到：默认不修改。
- 文件名：默认不修改，输入下载路径后将自动进行填充。

3. 单击【确认】。系统将自行下载 DIscuz！Q 部署文件，请耐心等待。

#### 步骤五：解压  DIscuz！Q 部署文件压缩包

1. 选择已下载好 Discuz！Q 部署文件压缩包，并单击【解压】。如下图所示：
![](https://main.qcloudimg.com/raw/a081ffd726de5bb6322404db1e28ba8d.png)

2. 在弹出的解压文件窗口中，单击【解压】。如下图所示：

![](https://main.qcloudimg.com/raw/71ae81ac89cf9b178ba2deb8e29c72c1.png)

::: tip
文件解压成功后，可自行删除 Discuz！Q 部署文件压缩包，不影响 Discuz！Q 的正常使用。
::: 

#### 步骤六：修改网站配置
##### 修改运行环境

1. 在宝塔控制台中，单击【网站】，并选择已创建 Discuz！Q  站点，单击【设置】。如下图所示： 

![](https://main.qcloudimg.com/raw/db50d6b8b321727e62058dd7737c67fe.png)

2. 在弹出的站点修改窗口中，单击【网站目录】，进行网站目录相关设置。如下图所示： 

![](https://main.qcloudimg.com/raw/31486b4566bdd0ea4037f61eb72ec587.png)

3. 运行目录勾选`/public`。如下图所示:

![](https://main.qcloudimg.com/raw/e80a52489b3de196d2bb4121d61d8c1a.png)

4. 单击【保存】。

##### 配置伪静态

::: tip
如果使用 `Apache `服务，此步操作无需配置；如果使用 `Nginx` 服务，请按照如下进行设置伪静态。
::: 

1. 进入【网站】，单击刚才添加站点，操作栏处【设置】。
2. 弹出的【站点修改】窗口中，单击【伪静态】，将以下内容复制粘贴进去，并单击【保存】。

```
location / {
  try_files $uri $uri/ /index.php?$query_string;
}
```
#####  设置 gzip
::: tip
如果使用Apache服务，此步操作无需配置；如果使用 Nginx 服务，请按照如下进行设置 gzip。
::: 

1. 单击【软件商城】，并查找到您安装的 Nginx。
2. 单击 Nginx 操作栏的【设置】。
3. 在弹出的【nginx 管理】窗口中，单击【配置修改】。
4. 在【配置修改】中查找到 `gzip_types` 字段并在前端添加 `application/json` 和末尾处添加 `application/vnd.api+json` 代码。如下图所示：

![](https://main.qcloudimg.com/raw/ebf3190907cd495ca22773c9c2c12e1f.png)

5. 单击 【性能调整】，在 gzip 处选择【开启】。若已开启请忽略此步操作。
6. 单击【保存】。重启 Nginx 服务。

如果您使用的是 `IIS` Web 服务，请单击站点修改窗口中【伪静态】，将以下内容复制粘贴进去，并单击【保存】。
```
<rule name="root_location_rewrite" stopProcessing="true">
  <match ignoreCase="false" url="."/>
  <conditions logicalGrouping="MatchAll">
    <add ignoreCase="false" input="{REQUEST_FILENAME}" matchType="IsFile" negate="true"/>
    <add ignoreCase="false" input="{REQUEST_FILENAME}" matchType="IsDirectory" negate="true"/>
  </conditions>
  <action appendQueryString="true" type="Rewrite" url="index.php?{QUERY_STRING}"/>
</rule>
<rule name="default_file_rewrite" stopProcessing="true">
  <match ignoreCase="false" url="^$"/>
  <action appendQueryString="true" type="Rewrite" url="index.php?{QUERY_STRING}"/>
</rule>
```

#### 步骤六：初始化安装 Discuz! Q

1. 打开本地浏览器，访问 `http://<绑定网站的域名名称>/dl.php` 。如下图所示：

![](https://main.qcloudimg.com/raw/d571ed3234565af42c508666b827ca37.png)


::: tip
- 站点如需部署 SSL 证书，使用 HTTPS 协议进行站点访问。请在以下操作之前部署安装 SSL 证书，并使用`https://<绑定网站的域名名称>/dl.php`  进行访问。部署安装 SSL 证书详情操作请参考： [宝塔服务器证书安装](https://cloud.tencent.com/document/product/400/50874)。
-  如需购买 SSL 证书，可参考 [SSL 证书购买流程](https://cloud.tencent.com/document/product/400/7995)，请根据您的实际情况进行购买相关证书。腾讯云为 Discuz! Q 站点提供免费 SSL 证书申请。详情请查看：[域名型（DV）免费证书申请流程](https://cloud.tencent.com/document/product/400/6814)。
:::

2. 单击【下一步】，Discuz! Q 将自行进行站点检查。如有报错等问题，请查看 [常见问题](https://discuz.com/docs/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98.html) 进行排查处理。

3. Discuz! Q 检查完成后，需配置网站相关信息。如下图所示：

![](https://main.qcloudimg.com/raw/8e3ac52669180620b946750787b50c95.png)

- **站点名称**：请输入您的站点名称信息，可自定义。
- **MySQL 服务器地址**：请输入您的 MySQL 服务器地址，如您使用宝塔创建的服务器本地数据库，请输入`127.0.0.1`即可。
- **数据库名称**：请输入您的数据库名称。如您使用宝塔创建的服务器本地数据库，可登录服务器的宝塔控制台，【数据库】进行查看。
- **MySQL 用户名**：请输入您的数据库用户名。如您使用宝塔创建的服务器本地数据库，可登录服务器的宝塔控制台，【数据库】进行查看。
- **MySQL 密码**：请输入您的数据库密码。如您使用宝塔创建的服务器本地数据库，可登录服务器的宝塔控制台，【数据库】进行查看。
- **表前缀**：可选，可自定义数据库表前缀名称。默认不填。
- **设置管理员用户名**：请输入您 Discuz! Q 站点的管理员用户名。
- **设置管理员密码**：请输入您 Discuz! Q 站点的管理员密码。
- **管理员密码确认**：请再次输入您 Discuz! Q 站点的管理员密码。
4. 单击【下一步】。即可完成 DIscuz！Q 的安装部署。

#### 步骤七：添加计划任务

为保证 DIscuz！Q 站点功能的正常使用。您还需要在宝塔面板中添加计划任务。

1. 登录宝塔控制台，单击【计划任务】，进入计划任务页面，并选择与填写相关信息。如下图所示：

![](https://main.qcloudimg.com/raw/1ee9cba4a3d9ed05b79f388992903f51.png)
- 任务类型：选择 Shell 脚本。
- 任务名称：可自定义，建议填写【Discuz！Q 计划任务】 便于区分。
- 执行周期：选择【N分钟】，并填写【1】分钟。
- 脚本内容：请输入以下命令,命令【网站主目录】请按实际情况做相应调整。

```
c:\btsoft\php\<PHP版本目录>\php.exe c:\wwwroot\<网站主目录>\disco schedule:run
```
2. 单击【添加任务】。




