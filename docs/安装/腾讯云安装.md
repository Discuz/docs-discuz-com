---
sidebarDepth: 2
---
# 腾讯云安装

## 云服务器 CVM

###  镜像（推荐）
####  操作场景
本文档将指导如何基于腾讯云（CVM）服务器镜像安装部署 Discuz! Q。
::: tip
基于官方提供的腾讯云镜像，可实现快速安装，简洁部署。
:::

####  前提条件
已 [注册腾讯云](https://cloud.tencent.com/document/product/1263/46191)账号并完成 [实名认证](https://cloud.tencent.com/document/product/378/3629)。  

####  操作步骤
#### 步骤一：新购或配置已有云服务器
#### 新购腾讯云服务器
1. 登录 [腾讯云云市场]( https://market.cloud.tencent.com/products/19943) 官方 Discuz! Q 镜像页面。
2. 在 [腾讯云云市场]( https://market.cloud.tencent.com/products/19943) 官方 Discuz! Q 镜像页面中，单击 【立即购买】。如下图所示：
![](https://main.qcloudimg.com/raw/1174ace28bd8b5c144064ac888331d19.png)
3. 在自定义购买页，请根据您的实际需求，选择存储介质、带宽、设置安全组等其他配置，并完成支付操作。自定义配置说明详情请查看：[自定义配置](https://cloud.tencent.com/document/product/213/8036)。
::: danger
镜像选项请勿修改，保持选择【镜像市场】、【Discuz! Q官方镜像 v2.0】。
::: 

#### 已有腾讯云服务器

::: tip
已购买腾讯云（CVM）服务器，并使用此方式进行镜像安装，会导致原服务器中的数据全部丢失。请做好数据备份或使用 [其它安装方式](https://discuz.com/docs/%E8%85%BE%E8%AE%AF%E4%BA%91%E5%AE%89%E8%A3%85.html#%E5%85%B6%E5%AE%83%E5%AE%89%E8%A3%85%E6%96%B9%E5%BC%8F) 进行安装。
:::

1. 登录 [腾讯云服务器控制台](https://console.cloud.tencent.com/cvm)，在实例的管理页面，找到需要部署安装的云服务器实例。
2. 单击【更多】，在展开的下拉框中，单击【重装系统】。如下图所示：
![](https://main.qcloudimg.com/raw/075771fb9a02f116578b42a5e52f7fa8.png)
3. 在弹出的重装系统须知中，单击【确认，开始重装】。
4. 在重装系统的窗口中，选择 Discuz! Q 官方镜像。如下图所示：

![](https://main.qcloudimg.com/raw/330e68f6b9ca9292ed257cbaf2116916.png)

- **镜像来源**：勾选【服务市场】。
- **镜像**：勾选【建站系统】。并在下拉框中选择【Discuz! Q官方镜像】。
5. 设置密码后，点击【开始重装】即可。


#### 步骤二：系统管理与配置

1. 安装完成后，您可以打开本地浏览器，访问 `http://<您的服务器外网 IP 地址>` 查看您的 Discuz! Q 网站。并且您可以登录您的云服务器终端。在终端中使用 `dzqinfo` 命令，查看 Discuz! Q 和宝塔相关信息进行管理与配置。

::: tip
-  Discuz! Q 镜像基于宝塔面板搭建制作，您可以浏览器访问 `http://<服务器外网 IP 地址>:8888` 进入宝塔面板控制台。
-  如果宝塔面板或 Discuz! Q  无法访问，请检查您的 [安全组设置](https://cloud.tencent.com/document/product/213/15377)。
-  如果您需要修改 Discuz! Q 的配置域名，请参考 [常见问题](https://discuz.com/docs/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98.html#%E5%A6%82%E4%BD%95%E6%9B%B4%E6%94%B9%E5%9F%9F%E5%90%8D)。
::: 

###  其它安装方式
详情请查看：[Windows 主机安装](https://discuz.com/docs/Windows%20%E4%B8%BB%E6%9C%BA.html)、[Linux主机安装](https://discuz.com/docs/Linux%20%E4%B8%BB%E6%9C%BA.html)。



## 轻量云服务器 Lighthouse 
### 镜像（推荐）
#### 操作场景
本文档将指导您如何基于腾讯云轻量云服务器 （Lighthouse）镜像安装部署 Discuz! Q。
::: tip
基于官方提供的腾讯云镜像，可实现快速安装，简洁部署。
::: 

#### 前提条件

已注册腾讯云账号并完成实名认证。  

#### 操作步骤
#### 新购腾讯轻量云服务器

#### 步骤一：购买腾讯轻量云（Lighthouse）服务器

1. 登录 [轻量应用服务器购买页](https://buy.cloud.tencent.com/lighthouse?buy_from=lh-console) 。
2. 在 [轻量应用服务器购买页]( https://market.cloud.tencent.com/products/19943) 页中，勾选相关配置信息。如下图所示：

![](https://main.qcloudimg.com/raw/b0973cd570bad2c0d1591ffeba58201f.png)

镜像：勾选【应用镜像】并选择 【Discuz! Q v1.0】。
::: tip
请根据您的实际需求，选择实例套餐、购买时长、地域、购买数量等其他配置。
::: 

3. 单击【立即购买】。完成支付操作。

#### 已有腾讯轻量云（Lighthouse）服务器

::: tip
已购买腾讯轻量云（Lighthouse）服务器，并使用此方式进行镜像安装，会导致原服务器中的数据全部丢失。请做好数据备份或使用 [其它安装方式](https://discuz.com/docs/%E8%85%BE%E8%AE%AF%E4%BA%91%E5%AE%89%E8%A3%85.html#%E5%85%B6%E5%AE%83%E5%AE%89%E8%A3%85%E6%96%B9%E5%BC%8F-2) 进行安装。

::: 

1. 登录 [轻量应用服务器控制台](https://console.cloud.tencent.com/lighthouse/instance/index)，在实例的管理页面，找到需要部署安装的轻量应用服务器实例。
2. 单击【更多】，在展开的下拉框中，单击【管理】。
3. 在管理页中，单击并选择【应用管理】>【重置应用】。如下图所示：

![](https://main.qcloudimg.com/raw/6843f82100f3c394c4a25062a3207713.png)

4. 在重置应用的窗口中，选择 Discuz! Q 官方镜像。如下图所示：

![](https://main.qcloudimg.com/raw/80c9920368c649953ad50963757e7dfc.png)
- **镜像类型**：勾选【应用镜像】。
- **选择镜像**：勾选【Discuz! Q v1.0】。
5. 勾选【确认已了解以上内容，我确定已备份完成】，并单击【确实】即可完成已有腾讯轻量云（Lighthouse）服务器部署操作。



#### 步骤二：系统管理

安装完成后，您可以进入腾讯云轻量应用服务器实例的管理页【应用管理】，在【应用内软件信息】卡片中查看相关信息并进行管理。

::: tip
-  Discuz! Q 镜像基于宝塔面板搭建制作，您可以浏览器访问 `http://<服务器外网 IP 地址>:8888` 进入宝塔面板控制台。
-  您可以登录您的服务器，在终端中执行命令 `dzqinfo` 查看 Discuz! Q 和宝塔的管理员用户名与密码等相关信息。
- 如果您需要修改 Discuz! Q 的配置域名，请参考 [常见问题](https://discuz.com/docs/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98.html#%E5%A6%82%E4%BD%95%E6%9B%B4%E6%94%B9%E5%9F%9F%E5%90%8D)。
::: 

###  其它安装方式
详情请查看：[Windows 主机安装](https://discuz.com/docs/Windows%20%E4%B8%BB%E6%9C%BA.html)、[Linux主机安装](https://discuz.com/docs/Linux%20%E4%B8%BB%E6%9C%BA.html)


#### 部署异常

安装过程中可能遇到一些异常，请根据异常提示查看以下解决方案进行解决。如果遇到无法解决的问题，请 [提交工单](https://console.cloud.tencent.com/workorder/category) 联系我们，我们将协助您解决问题。
![安装异常](https://main.qcloudimg.com/raw/1e8022496f7c221a6f84fa02ddd5859e.png)

#### 账户余额不足

Discuz!Q 应用创建时，会一同创建云数据库 CynosDB for Mysql。创建数据库资源会预先扣1个小时的费用，为了保证服务的可用，推荐先 [充值腾讯云账户](https://console.cloud.tencent.com/expense/recharge) 5元钱。

#### 云接入根路径已经被占用

Discuz! Q 将会占用根路径，如果当前环境被占用，推荐再创建一个按量计费环境进行安装。

#### CynosDB 被隔离

错误信息 `queryClusterDetail failed, err=DescribeClusters invalid response.detail.status[isolated]`，代表 CynosDB 集群被隔离，请前往回收站将该集群恢复或者直接删除。

#### 共享文件存储 CFS 资源售罄

错误信息：“参数值错误：该地域无法提供服务”，代表共享文件存储 CFS 该地域可用区售罄。我们将及时补货，请耐心等待。

#### 常见问题

#### 小程序部署

小程序的部署需要单独提审，安装完本扩展应用后，需要参考 [小程序部署指南](https://discuz.com/docs/%E5%B0%8F%E7%A8%8B%E5%BA%8F.html) 进行构建发布小程序端的代码。

#### 公众号白名单 IP 配置

公众号登录开通时，需要将服务的 IP 添加到公众号的 IP 白名单中，详情请参见 [第三方登录设置](https://discuz.com/manual-admin/2.html#_2-3-%E7%AC%AC%E4%B8%89%E6%96%B9%E7%99%BB%E5%BD%95%E8%AE%BE%E7%BD%AE) 文档。

1. 在扩展应用详情页 API 和资源模块中，单击云托管的服务详情。
   ![查看云托管服务](https://main.qcloudimg.com/raw/e210b5f23304867805926e3c3a4bb07e.png)
2. 单击服务配置，查看服务的出口 NAT IP。
   ![NAT IP](https://main.qcloudimg.com/raw/e9b4724b662b52230b7ce5ca5ff270db.png)

#### 版本升级

云开发会定期跟踪 Discuz! Q 的大的版本更新，为您推送版本升级，可一键完成升级操作。

#### 复用已有的 CynosDB 集群

本应用会选取当前环境所处地域，并且在同一个 vpc 下，集群名为 DiscuzCynosDB 的数据库实例。如果不存在，则会创建新的集群。

如果希望复用已有 CynosDB 集群，可参考如下步骤。

1. 查看 CynosDB 所在的私有网络，并且将集群名更改为 DiscuzCynosDB。
   ![](https://main.qcloudimg.com/raw/3549cec77f92046bada85b9ab79f05e7.png)
2. 创建一个新的按量计费环境，并且开通云托管，选择自定义配置，勾选 CynosDB 所在的私有网络，默认请勾选所有子网。
   ![](https://main.qcloudimg.com/raw/eada2b2dac2ee060380da78055da1b5b.png)
3. 回到扩展应用页面安装 Discuz!Q。

#### 其他

#### 程序配置信息

您可以通过以下配置参数：

- 环境 ID：选择需要部署的环境，在哪个环境下使用。
- 管理员用户名：Discuz! Q 后台管理系统的管理员用户名，默认为 admin。
- 管理员密码：Discuz! Q 后台管理系统的管理员密码，第一次安装时，会设置并加密存储在数据库内，之后变更请前往管理系统的用户管理面板重置密码，详情请参见 [用户管理](https://discuz.com/manual-admin/3.html#_3-1-%E7%94%A8%E6%88%B7%E7%AE%A1%E7%90%86)。
- 数据库用户名：默认为 root，当前不可修改。
- 数据库密码：root 账号的密码，初次安装时设置后将作为初始化的密码创建 CynosDB。后续修改密码请前往 CynosDB 控制台的账号管理页面，修改密码后，请同步修改此处的密码，以保证数据库的正常使用。

#### 计费

此能力使用云开发与其他腾讯云服务，可能会产生相关费用。云开发与云上其他资源分开计费，您可以在 [费用中心](https://console.cloud.tencent.com/expense/overview) 查看具体信息。

1. 云托管（[产品定价](https://cloud.tencent.com/document/product/1243/47823) 及 [使用明细](https://console.cloud.tencent.com/tcb)）。
2. 静态网站托管（[产品定价](https://cloud.tencent.com/document/product/1210/42854) 及 [使用明细](https://console.cloud.tencent.com/tcb)）。
3. 文件存储（Cloud File Storage，CFS）（[产品定价](https://cloud.tencent.com/document/product/582/47378) 及 [使用明细](https://console.cloud.tencent.com/cfs/overview)）。
4. 云数据库 CynosDB for MySQL（[产品定价](https://cloud.tencent.com/document/product/1003/30493) 及 [使用明细](https://console.cloud.tencent.com/cynosdb)）。
